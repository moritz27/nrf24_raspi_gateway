# NRF24_Raspi_Gateway

Running a NRF24 Gateway on a Raspberry to receive (Sensor-) Data from different nodes. The Repository for the Nodes can be found [here]().

## Setup

### Hardware

All hardware that is needed to run the gateway is a _Raspberry Pi_ and a _NRF24L01_ Module.

The following connections need to made between the Pi and the NRF Module:

![nRF24L01-Pinout.jpg](doc/nRF24L01-Pinout.jpg)

| NRF24L01+ | Raspberry Pi     |
| --------- | ---------------- |
| GND       | 25 GND           |
| VCC       | 17 3.3V          |
| CE        | 22(GPIO25)       |
| CS        | 24 (GPIO8/CE0)   |
| SCK       | 23 (GPIO11/SCKL) |
| MOSI      | 19 (GPIO10/MOSI) |
| MISO      | 21 (GPIO09/MISO) |
| IRQ       | 18 (GPIO24)      |

CE and IRQ can be changed in the section `["nrf"]["pins"]` in the  __nrf24_gateway_config.json__ file.


### Software

#### Enable SPI 

The Raspberry Pi needs activated SPI:

```
sudo raspi-config
```

```
„Advanced“ -> „SPI“
```

#### GPIO Interface

The Gateway is using the [pigpio](https://abyz.me.uk/rpi/pigpio/)  libary to interact with the __nrf24l01__ module over the SPI Bus. 

- Start the pigpio daemon: `sudo pigpiod`
- Restart the pigpio daemon: `sudo pigpiod restart`
- Stop the pigpio daemon: `sudo killall pigpiod`
- Status: `sudo systemctl status pigpiod`

#### Python

The nrf communication depends on the [py-nrf24](https://github.com/bjarne-hansen/py-nrf24) libary.

To setup a virtual python environment and install all needed depencies simply run the `setup.sh` script in the setup folder.

If any new libaries are added to the project the command `nrf_env/bin/python3 -m pip freeze > $(pwd)/setup/nrf24_raspi_gateway_requirements.txt` will update the textfile.

To run the gateway in background while the output gets piped to the __gateway.log__ file use the command

```
nohup nrf_env/bin/python3 -u nrf24_raspi_gateway.py &> nrf24_raspi_gateway.log &
```

#### Optional: Process Manager 2

Process Manager 2 (PM2) is a tool to manage the execution of a script. It can be started automatically at defined events for example at the boot of the system and restart the command if it crashes.

To install PM2 first the Node Package Manager (NPM) is needed:

```bash
sudo apt install npm
```

After that the package can be installed with the command:

```bash
sudo npm install pm2@latest -g
```

To automatically [start PM2 at boot](https://pm2.keymetrics.io/docs/usage/startup/):

```bash
pm2 startup
```

To configurate the behavior of the task the `ecosystem.config.json` can be edited according to [the Docs](https://pm2.keymetrics.io/docs/usage/application-declaration/) and for [Logs](https://pm2.keymetrics.io/docs/usage/log-management/):

Finally run the command to start the script with PM2:

```bash
pm2 start setup/ecosystem.config.json
```

Restart the application and toggle watch status

```bash
pm2 restart nrf24_raspi_gateway --watch
```

Add watched file(s) to `ecosystem.config.json` by add:

```bash
      "watch": ["nrf24_raspi_gateway.py"],
      "watch_delay": 1000
```

Save the current app list:

```bash
pm2 save
```

To stop it aggain:

```bash
pm2 stop nrf24_raspi_gateway
```

Constraing the size of the created log files using [logrotate](https://github.com/keymetrics/pm2-logrotate)

```bash
pm2 install pm2-logrotate
```

```bash
pm2 set pm2-logrotate:retain 5
pm2 set pm2-logrotate:dateFormat DD-MM-YYYY_HH-mm-ss
pm2 set pm2-logrotate:max_size 10K
```

Clear all log files (for a specific task)

```bash
pm2 flush
pm2 flush nrf24_raspi_gateway
```


## Receiving Messages

The default RX-Adress is 0xF0F0F0F066. More pipes can be added using the array `["nrf"]["rx_addresses"]` in the  __nrf24_gateway_config.json__ file.


## Decode the content of Messages

The content is transmitted in binary format to reduce the amount of data beeing transmitted. 

Each message is containing a header of 4 Bytes consisting of:
- __Node ID__ 1 Byte
- __Config Register__ 1 Byte
- an optional __Unix Timestamp__ 4 Byte

And 3 Bytes for each signal:
- __Signal_Type__ 1 Byte
- __Signal_Value__ 2 Byte

1 to 8 signals can be transmitted in one message. 

The following table shows the content of a example message containing two signals:

| Byte    | 0       | 1               | 2                  | 3                   | 4                    | 5                    | 6             | 7              | 8                       | 9             | 10             | 11                      |
| ------- | ------- | --------------- | ------------------ | ------------------- | -------------------- | -------------------- | ------------- | -------------- | ----------------------- | ------------- | -------------- | ----------------------- |
| Name    | Node ID | Config Register | UNIX Timestamp UTC |                     |                      |                      | Signal Type 1 | Signal Value 1 |                         | Signal Type 2 | Signal Value 2 |                         |
| Length  | 1 Byte  | 1 Byte          | 4 Byte             |                     |                      |                      | 1 Byte        | 2 Byte         |                         | 1 Byte        | 2 Byte         |                         |
| Content | 0-255   | mixed           | TS[0]  & 0xFF      | (TS[1] >> 8) & 0xFF | (TS[2] >> 16) & 0xFF | (TS[3] >> 24) & 0xFF | 0-255         | Value1[0] 0xFF | (Value1[1] >> 8) & 0xFF | 0-255         | Value2[0] 0xFF | (Value2[1] >> 8) & 0xFF |

### Content of Config Register

| Config Register |                  |     |     |                       |      |      |      |      |
| --------------- | ---------------- | --- | --- | --------------------- | ---- | ---- | ---- | ---- |
| Bit             | 0                | 1   | 2   | 3                     | 4    | 5    | 6    | 7    |
| Name            | Number of Values |     |     | Timestamp Transmitted | n.a. | n.a. | n.a. | n.a. |
| Value           | 1-8              |     |     | True/False [bool]     | 0xFF | 0xFF | 0xFF | 0xFF |

### Types of Signals

Currently 4 Types of Signals are defined but can be expanded without problem.

| Signal Type  | Dec Value |
| ------------ | --------- |
| None         | 0         |
| internal ADC | 1         |
| Temperature  | 2         |
| Humidity     | 3         |
| Air Pressure | 4         |
| Distance     | 5         |
|              | ...       |

To do so a entry has to be added to the `["signal_types"]` section in the __nrf24_gateway_config.json__:

```json
"key": {
    "name": "Distance",
    "unit": "m",
    "conversion": 1000
}
```

- __key__ (e.g. "5" ) is the integer number that the node is also transmitting
- __name__ is the name of the signal
- __unit__ the _SI_ Base unit
- __conversion__ the factor to devide the received value

The conversion is applied automatically if a certain known signal is received.

## Pipe Message Content

In the current state the content of the received messages can be piped to one of the following:

### MQTT

If the value `["MQTT"]["send_msg"]` in the __nrf24_gateway_config.json__ is `True` the content is getting published to the defined MQTT Server on the IP `["MQTT"]["server_ip"]`.

The used __topic__ is consisting of the  `["MQTT"]["base_topic"]`, the received __Node ID__ and the __Sensor Type__ of the sensor.

For example: "nrf_gateway/2/BME280"

The content is created by converting the dictonary of the Signals into a String with `json.dumps(signals)`:

For example: 
```json
{
    "Temperature": 19.6,
    "Humidity": 55.1,
    "Air_Pressure": 996.0
}
```
### InfluxDB

If the value `["InfluxDB"]["send_msg"]` in the __nrf24_gateway_config.json__ is `True` the content is getting published to the defined InfluxDB2 Server. 

The configuration is stored in the "credentials" folder inside the "influxDB2.json" a example is looking like this:

```json
{
    "url": "http://192.168.178.11:8086",
    "token": "theTokenWithWritingPermissions",
    "org": "yourOrgName"
}
```

Every time a message is received the data is getting saved to the InfluxDB Database. 

- The __bucket__ and the __measurement__ are defined in the `["InfluxDB"]` section in the __nrf24_gateway_config.json__.
- All __tags__ are from the Node itself as defined in the `["sensor_nodes"]` section in the __nrf24_gateway_config.json__:
  - Location
  - Type
  - Node_ID
- The __fields__ are filled with the pair-values received from the node. As shown in the MQTT Section.