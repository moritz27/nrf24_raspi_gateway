from nrf24 import *
import pigpio
import time
import json
import traceback
import os
import psutil
import hashlib
from datetime import datetime
import paho.mqtt.publish as publish
from influxdb_client import InfluxDBClient
from influxdb_client.client.write_api import SYNCHRONOUS


class NrfGateway:
    def __init__(self, gateway_config_file_name="nrf24_raspi_gateway_config.json"):
        ''' Init the NRF Gateway from config file '''
        print("Starting NRF24 Gateway")
        self.__kill_other_running_scripts()
        self.__run_command_if_not_active("pigpiod", sudo=True)
        self.script_dir = os.path.dirname(os.path.realpath(__file__))
        self.influx2_config_path = os.path.join(
            self.script_dir, "credentials", "influxDB2.json")
        self.gateway_config_file_name = gateway_config_file_name
        self.config = {}
        self.last_config_file_hash = None
        self.update_config_from_file()
        if self.config != 1:
            self.pi = pigpio.pi("localhost", 8888)
            self.setup_nrf_gateway()
        else:
            print("config file not found.. setup failed")

    def __kill_other_running_scripts(self):
        ''' Terminate all other scripts with the same name as this one to ensure that only this instance of this script is running.'''
        script_name = os.path.basename(__file__)
        for proc in psutil.process_iter():
            command = " ".join(proc.cmdline())
            if script_name in command:
                if proc.pid != os.getpid():
                    print("Terminate:", command, "with pid:", proc.pid)
                    proc.terminate()

    def __run_command_if_not_active(self, command, sudo=False):
        ''' Run the specified command if it is not running already. '''
        if not (self.__check_if_process_running(command)):
            if sudo:
                command = "sudo " + command
            print("Running command:", command)
            os.system(command)
            time.sleep(1)
        else:
            print(command, "already running")

    def __check_if_process_running(self, processName):
        ''' Check if there is any running process that contains the given name processName.'''
        # Iterate over the all the running process
        for proc in psutil.process_iter():
            try:
                # Check if process name contains the given name string.
                if processName.lower() in proc.name().lower():
                    return True
            except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
                pass
        return False

    def __sha1(self, filename):
        BUF_SIZE = 65536  # read stuff in 64kb chunks!
        sha1 = hashlib.sha1()
        with open(filename, 'rb') as f:
            while True:
                data = f.read(BUF_SIZE)
                if not data:
                    break
                sha1.update(data)
        return sha1.hexdigest()

    def __update_variable_from_file(self, filename, last_hash=None):
        new_hash = self.__sha1(filename)
        if new_hash != last_hash:
            variable = self.read_json_file(filename)
            print(filename, "has updated!")
            return True, variable, new_hash
        return False, None, None

    def update_config_from_file(self):
        update, config, last_config_file_hash = self.__update_variable_from_file(self.gateway_config_file_name, self.last_config_file_hash)
        if update:
            self.config = config
            self.last_config_file_hash = last_config_file_hash

    def setup_nrf_gateway(self):
        ''' Setting up the NRF24 Gateway from loaded configuration'''
        ce_pin = self.config["nrf"]["pins"]["ce"]
        self.nrf = NRF24(self.pi, ce=ce_pin, payload_size=RF24_PAYLOAD.DYNAMIC,
                         channel=115, data_rate=RF24_DATA_RATE.RATE_250KBPS, pa_level=RF24_PA.MIN)

        print("Start opening reading pipes from config file:")
        for pipe_num, rx_ad in enumerate(self.config["nrf"]["rx_addresses"], start=0x0B):
            if pipe_num > 0x0F:
                print("Only 5 reading pipes available, all others will be ignored")
                break
            print(hex(pipe_num), hex(int(rx_ad, 16)))
            self.nrf.open_reading_pipe(pipe_num, int(rx_ad, 16))
        self.nrf.show_registers()
        self.nrf.flush_rx()
        irq_pin = self.config["nrf"]["pins"]["irq"]
        self.pi.callback(irq_pin, pigpio.FALLING_EDGE, self.receive_interrupt)
        print("Setup finished wating for incoming messages..")

    def read_json_file(self, filename="nrf24_raspi_gateway_config.json"):
        ''' Returns the parsed content of a specified JSON file'''
        try:
            path = os.path.join(self.script_dir, filename)
            print("reading json from:", path)
            if os.path.exists(path):
                f = open(path, "r")
                data = json.loads(f.read())
                f.close()
                return data
            else:
                return 1
        except Exception as e:
            print("Error reading file: ", path, ": ", e)
            return 1

    def receive_interrupt(self, gpio, level, tick):
        ''' The command that gets executed whenever a new message is received. '''
        # print(f'Interrupt: gpio={gpio}, level={("LOW", "HIGH", "NONE")[level]}, tick={tick}')
        while self.nrf.data_ready():
            time.sleep(0.1)
            try:
                receive_time = datetime.now()
                now_ts = int(round(receive_time.timestamp()))
                pipe = self.nrf.data_pipe()
                payload = self.nrf.get_payload()
                received_id = int(payload[0])
                config = int(payload[1])
                signal_count = config & 0b111
                has_timestamp = config & 0b1000
                value_start_index = 2
                signals = {}
                if (has_timestamp):
                    value_start_index += 4
                    rtc_timestamp = 0
                    for i in range(4):
                        rtc_timestamp += payload[2 + i] << (8 * i)
                    signals["RTC Timestamp"] = rtc_timestamp
                    signals["RTC Difference"] = now_ts - rtc_timestamp

                for i in range(signal_count):
                    signal_type_num = str(
                        int(payload[value_start_index + i * 3]))
                    low_byte = int(payload[value_start_index + 1 + i * 3])
                    high_byte = int(
                        payload[value_start_index + 2 + i * 3]) << 8
                    value = low_byte + high_byte
                    if "conversion" in self.config["signal_types"][signal_type_num]:
                        conv_factor = self.config["signal_types"][signal_type_num]["conversion"]
                        value = round(value / conv_factor, 3)
                    signals[self.config["signal_types"]
                            [signal_type_num]["name"]] = value

                sensor_type = self.config["sensor_nodes"][str(
                    received_id)]["type"]
                location = self.config["sensor_nodes"][str(
                    received_id)]["location"]

                if self.config["MQTT"]["send_msg"] == True:
                    topic = self.config["MQTT"]["base_topic"] + \
                        received_id + "/" + sensor_type
                    payload = json.dumps(signals)
                    print("publishing MQTT message to topic:", topic)
                    publish.single(topic=topic, payload=payload,
                                   hostname=self.config["MQTT"]["server_ip"], client_id=self.config["id"])

                if self.config["InfluxDB"]["send_msg"] == True:
                    if "bucket_name" in self.config["sensor_nodes"][str(received_id)]:
                        bucket_name = self.config["sensor_nodes"][str(
                            received_id)]["bucket_name"]
                    else:
                        bucket_name = self.config["InfluxDB"]["default_bucket_name"]

                    if "measurement" in self.config["sensor_nodes"][str(received_id)]:
                        measurement = self.config["sensor_nodes"][str(
                            received_id)]["measurement"]
                    else:
                        measurement = self.config["InfluxDB"]["default_measurement"]

                    client = InfluxDBClient.from_config_file(
                        self.influx2_config_path)
                    write_api = client.write_api(write_options=SYNCHRONOUS)
                    dictionary = {
                        "measurement": measurement,
                        "tags": {"Location": location, "Type": sensor_type, "ID": received_id},
                        "fields": signals,
                    }
                    write_api.write(bucket_name, record=dictionary)
                    write_api.close()

                # print(f"{receive_time:%d-%m-%Y %H:%M:%S}: RX Pipe: {pipe}, Sensor ID: {received_id}, Type: {sensor_type}, Location: {location}, Number of Bytes: {len(payload)}, value: {signals}")
                print(
                    f"RX Pipe: {pipe}, Sensor ID: {received_id}, Type: {sensor_type}, Location: {location}, Number of Bytes: {len(payload)}, value: {signals}")
            except:
                traceback.print_exc()


my_gateway = NrfGateway()

try:
    while True:
        time.sleep(5)
        my_gateway.update_config_from_file()
        # my_gateway.nrf.show_registers()
except:
    traceback.print_exc()

finally:
    print("Exiting now")
    my_gateway.nrf.power_down()
    time.sleep(1)
    my_gateway.pi.stop()
